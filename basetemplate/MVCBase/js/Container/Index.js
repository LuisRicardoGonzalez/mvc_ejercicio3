﻿$(document).ready(function () {


    $("#tablacontainer").DataTable(
      { dom: 'Bfrtip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });

    var table = $('#tablacontainer').DataTable();


    $('#tablacontainer tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');

    });


    $('#botoncopiarcontainer').click(function () {

        var listadata = new Array();

        var objeto;

        table.rows(".selected").eq(0).each(function (index) {
            var row = table.row(index);
            var data = row.data();

            var obj = new Object();
            obj = {
                equipmentNbr: data[1],
                typeIso: data[2],
                own: data[3],
                typeLength: data[4],
                tareWt: data[5],
                safeWt: data[6]   
            };

            objeto = obj;
            listadata.push(objeto);
        });


        //////////////////////////////////////////////////////////////
        $.ajax({
            url: '/Principal/Copiartablacontainer',
            contentType: "application/json",
            type: "POST",
            datatype: "json",
            data: JSON.stringify(listadata),

            success: function (data) {
                if (data.ok = true) {
                    alert("Todo Bien");
                }
            }
        });

    });


});