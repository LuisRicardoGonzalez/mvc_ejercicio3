﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCBase.DB;
using MVCBase.Models.Container;
using MVCBase.Models.VesselVisit;
using MVCBase.Models.Documentos;
using System.IO;

namespace MVCBase.Controllers
{
    public class PrincipalController : Controller
    {
        // GET: Principal
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult VesselVisitPartial()
        {
            using (var dbocontextkoala3 = new Koala3Entities11())
            {
                var listaVesselVisits = new List<VesselVisitViewModel>();
                foreach (var filaTablaVessel in dbocontextkoala3.VesselVisits)
                {
                    listaVesselVisits.Add(AutoMapper.Mapper.Map<DB.VesselVisit, VesselVisitViewModel>(filaTablaVessel));
                }
                return PartialView(listaVesselVisits);
            }
        }

        [HttpGet]
        public ActionResult ContainerPartial()
        {
            using (var dbocontextkoala3 = new Koala3Entities11())
            {
                var listacontainers = new List<ContainerViewModel>();
                foreach (var filaTablaContainer in dbocontextkoala3.Containers)
                {
                    listacontainers.Add(AutoMapper.Mapper.Map<DB.Container, ContainerViewModel>(filaTablaContainer));
                }
                return PartialView(listacontainers);
            }
        }

        [HttpGet]
        public ActionResult CreateVesselVisit()
        {
            return PartialView(new VesselVisitViewModel());
        }

        [HttpPost]
        public ActionResult CreateVesselVisit1( VesselVisitViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var dbocontextkoala3 = new Koala3Entities11())
                {
                    dbocontextkoala3.VesselVisits.Add(AutoMapper.Mapper.Map<VesselVisitViewModel, DB.VesselVisit>(model));
                    dbocontextkoala3.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return PartialView(model);
        }




        [HttpPost]
        public ActionResult Copiartablavessel(List<VesselVisitViewModel> lista)
        {
                using (var dbocontextkoala3 = new Koala3Entities11())
                {
                    foreach(var vessel in lista)
                    {
                        dbocontextkoala3.VesselVisitCopias.Add(AutoMapper.Mapper.Map<VesselVisitViewModel, DB.VesselVisitCopia>(vessel));
                    }
                    dbocontextkoala3.SaveChanges();
                    return RedirectToAction("Index");
                }           
            
        }

        [HttpPost]
        public ActionResult Copiartablacontainer(List<ContainerViewModel> lista)
        {
                using (var dbocontextkoala3 = new Koala3Entities11())
                {
                    foreach (var container in lista)
                    {
                        dbocontextkoala3.ContainerCopias.Add(AutoMapper.Mapper.Map<ContainerViewModel, DB.ContainerCopia>(container));
                    }
                    dbocontextkoala3.SaveChanges();
                    return RedirectToAction("Index");
                } 
        }

        [HttpGet]
        public ActionResult VerVessel(int id)
        {
            using (var dbocontextkoala3 = new Koala3Entities11())
            {
                return View(AutoMapper.Mapper.Map<DB.VesselVisit, VesselVisitViewModel>((from a in dbocontextkoala3.VesselVisits where a.id == id select a).First()));
            }
        }

        [HttpPost]
        public ActionResult SubirDocumento(HttpPostedFileBase[] fileUpload, int id)
        {
            if (fileUpload.Any(x=>x == null)) return RedirectToAction("VerVessel", "Principal", new { id });            
            using (var dbocontextkoala3 = new Koala3Entities11())
            {
                foreach (var itemdelistadearchivos in fileUpload)
                { 
                    var modeloTDocumento = AutoMapper.Mapper.Map<HttpPostedFileBase, DB.TDocumento1>(itemdelistadearchivos);
                    modeloTDocumento.idVessel = id;
                    dbocontextkoala3.TDocumento1.Add(modeloTDocumento);
                }
                dbocontextkoala3.SaveChanges();
            }
            return RedirectToAction("VerVessel", "Principal", new { id });
        }

        [HttpGet]
        public ActionResult VerDocumentos(int id)
        {
            using (var dbocontextkoala3 = new Koala3Entities11())
            {
                var listaDeDocumentosEnVesselVisit = new List<DocumentoViewModel>();
                foreach (var filadetablaDocumentos in dbocontextkoala3.TDocumento1.Where(x => x.idVessel == id))
                {
                    listaDeDocumentosEnVesselVisit.Add(AutoMapper.Mapper.Map<DB.TDocumento1, DocumentoViewModel>(filadetablaDocumentos));                    
                }
                return PartialView(listaDeDocumentosEnVesselVisit);
            }       
        }

        [HttpGet]
        public ActionResult AbrirDocumento(int id)
        {
            using (var dbo = new Koala3Entities11())
            {
                var filaDocumentoVesselVissit = (from a in dbo.TDocumento1 where a.idDocumento == id select a).First();
                return File(filaDocumentoVesselVissit.Contenido, filaDocumentoVesselVissit.TipoDocumento);
            }
        }


        [HttpGet]
        public ActionResult ViewModal(int id)
        {
            using (var dbo = new Koala3Entities11())
            {
                var filadeTablaDocumento = (from a in dbo.TDocumento1 where a.idDocumento == id select a).First();
                return PartialView(AutoMapper.Mapper.Map<DB.TDocumento1, DocumentoViewModel>(filadeTablaDocumento));
            }
        }

         
        }


    }
