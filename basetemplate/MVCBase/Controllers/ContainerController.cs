﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCBase.Controllers
{
    public class ContainerController : Controller
    {
        [HttpGet]
        public ActionResult Index2()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CrearContacto()
        {
            return PartialView(new CrearContactoViewModel());
        }
        [HttpPost]
        public ActionResult CrearContacto(CrearContactoViewModel modelo)
        {
            return Json(new { ok = true });
        }

        // GET: Container
        public ActionResult Index()
        {
            return View(new ContainerController());
        }

        // GET: Container/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Container/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Container/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Container/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Container/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Container/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Container/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public class CrearContactoViewModel
        {
            public string NombreContacto { get; set; }
            public string Direccion { get; set; }
        }
    }
}
