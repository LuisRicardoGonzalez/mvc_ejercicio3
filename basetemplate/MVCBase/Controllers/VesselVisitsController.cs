﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCBase.Models.VesselVisit;
using MVCBase.DB;

namespace MVCBase.Controllers
{
    public class VesselVisitsController : Controller
    {
        // GET: VesselVisits
        public ActionResult Index()
        {
            using (var dbo = new Koala3Entities11())
            {

                var lista = new List<VesselVisitViewModel>();
                foreach (var line in dbo.VesselVisits)
                {
                    lista.Add(AutoMapper.Mapper.Map<DB.VesselVisit, VesselVisitViewModel>(line));
                }

                return View(lista);
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new VesselVisitViewModel());
        }

        [HttpPost]
        public ActionResult Create(VesselVisitViewModel model)
        {

            if (ModelState.IsValid)
            {
                using (var dbo = new Koala3Entities11())
                {
                    dbo.VesselVisits.Add(AutoMapper.Mapper.Map<VesselVisitViewModel, DB.VesselVisit>(model));
                    dbo.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(model);

        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var dbo = new Koala3Entities11())
            {
                var linea = (from a in dbo.VesselVisits where a.id == id select a).First();

                var model = AutoMapper.Mapper.Map<DB.VesselVisit, VesselVisitViewModel>(linea);

                return View(model);

            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VesselVisitViewModel vessel)
        {
            if (ModelState.IsValid)
            {

                using (var dbo = new Koala3Entities11())
                {


                    var model = AutoMapper.Mapper.Map<VesselVisitViewModel, DB.VesselVisit>(vessel);

                    var linea = (from a in dbo.VesselVisits where a.id == model.id select a).First();


                    if (linea != null)
                    {
                        // linea.id = model.id;
                        linea.visit = model.visit;
                        linea.line = model.line;
                        linea.vesselName = model.vesselName;
                        linea.phase = model.phase;
                        linea.ata = model.ata;
                        linea.atd = model.atd;
                        linea.eta = model.eta;
                        linea.etd = model.etd;
                        linea.serv = model.serv;
                        dbo.SaveChanges();
                    }

                    return RedirectToAction("Index");

                }
            }
            return View(vessel);

        }






    }
}