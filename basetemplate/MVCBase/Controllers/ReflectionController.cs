﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCBase.Models.VesselVisit;
using MVCBase.Reflection;
using System.Reflection;
using MVCBase.DB;
using System.ComponentModel.DataAnnotations;
using MVCBase.Models;


namespace MVCBase.Controllers
{      [atributo(NombreAmistoso = "Firulais")]
    public class ReflectionController : Controller
    {
        // GET: Reflection
        public ActionResult Index()
        {
            var T = Type.GetType("MVCBase.Controllers.ReflectionController").GetCustomAttributes(typeof(atributoAttribute), true);
            var displayname = T[0] as atributoAttribute;
            ViewBag.display = displayname.NombreAmistoso;

            using (var dbocontextkoala3 = new Koala3Entities11())
            {
                var lista = new List<VesselVisitViewModel>();
                foreach (var line in dbocontextkoala3.VesselVisits)
                {
                    lista.Add(AutoMapper.Mapper.Map<DB.VesselVisit, VesselVisitViewModel>(line));
                }
                return View(new List<PersonaViewModel> { new PersonaViewModel { Nombre = "Edwin", Apellido = "Zelaya", Edad = 22 } });
            }  
        }

        // GET: Reflection/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Reflection/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reflection/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Reflection/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Reflection/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Reflection/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Reflection/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }

    public class atributoAttribute : Attribute
    {
        public string NombreAmistoso;
        
        public atributoAttribute()
        { }

    }
}
