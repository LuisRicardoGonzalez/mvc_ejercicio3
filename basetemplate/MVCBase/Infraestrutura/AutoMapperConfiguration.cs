﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using MVCBase.Models.VesselVisit;
using MVCBase.Models.Container;
using MVCBase.Models.Documentos;

namespace MVCBase.Infraestrutura
{
    public class AutoMapperConfiguration
    {

        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                //x.CreateMap<CreateContainerViewModel, Container>()
                //    .ForMember(z => z.gkey, y => y.MapFrom(a => a.Gkey))
                //    .ForMember(z => z.equipmentNbr, y => y.MapFrom(a => a.EquipmentNbr))
                //    .ForMember(z => z.typeIso, y => y.MapFrom(a => a.TypeIso))
                //    .ForMember(z => z.own, y => y.MapFrom(a => a.Own))
                //    .ForMember(z => z.typeLength, y => y.MapFrom(a => a.TypeLength))
                //    .ForMember(z => z.tareWt, y => y.MapFrom(a => a.TareWt))
                //    .ForMember(z => z.safeWt, y => y.MapFrom(a => a.SafeWt));

                //x.CreateMap<Container, CreateContainerViewModel>()
                //   .ForMember(z => z.Gkey, y => y.MapFrom(a => a.gkey))
                //   .ForMember(z => z.EquipmentNbr, y => y.MapFrom(a => a.equipmentNbr))
                //   .ForMember(z => z.TypeIso, y => y.MapFrom(a => a.typeIso))
                //   .ForMember(z => z.Own, y => y.MapFrom(a => a.own))
                //   .ForMember(z => z.TypeLength, y => y.MapFrom(a => a.typeLength))
                //   .ForMember(z => z.TareWt, y => y.MapFrom(a => a.tareWt))
                //   .ForMember(z => z.SafeWt, y => y.MapFrom(a => a.safeWt));

                x.CreateMap<DB.VesselVisit, VesselVisitViewModel>()
                    .ForMember(z => z.id, y => y.MapFrom(a => a.id))
                    .ForMember(z => z.visit, y => y.MapFrom(a => a.visit))
                    .ForMember(z => z.line, y => y.MapFrom(a => a.line))
                    .ForMember(z => z.vesselName, y => y.MapFrom(a => a.vesselName))
                    .ForMember(z => z.phase, y => y.MapFrom(a => a.phase))
                    .ForMember(z => z.ata, y => y.MapFrom(a => a.ata))
                    .ForMember(z => z.atd, y => y.MapFrom(a => a.atd))
                    .ForMember(z => z.eta, y => y.MapFrom(a => a.eta))
                    .ForMember(z => z.etd, y => y.MapFrom(a => a.etd))
                    .ForMember(z => z.serv, y => y.MapFrom(a => a.serv));

     

                x.CreateMap<VesselVisitViewModel, DB.VesselVisit>()
                     .ForMember(z => z.id, y => y.MapFrom(a => a.id))
                     .ForMember(z => z.visit, y => y.MapFrom(a => a.visit))
                     .ForMember(z => z.line, y => y.MapFrom(a => a.line))
                     .ForMember(z => z.vesselName, y => y.MapFrom(a => a.vesselName))
                     .ForMember(z => z.phase, y => y.MapFrom(a => a.phase))
                     .ForMember(z => z.ata, y => y.MapFrom(a => a.ata))
                     .ForMember(z => z.atd, y => y.MapFrom(a => a.atd))
                     .ForMember(z => z.eta, y => y.MapFrom(a => a.eta))
                     .ForMember(z => z.etd, y => y.MapFrom(a => a.etd))
                     .ForMember(z => z.serv, y => y.MapFrom(a => a.serv));

                x.CreateMap<VesselVisitViewModel, DB.VesselVisitCopia>()
                     .ForMember(z => z.id, y => y.MapFrom(a => a.id))
                     .ForMember(z => z.visit, y => y.MapFrom(a => a.visit))
                     .ForMember(z => z.line, y => y.MapFrom(a => a.line))
                     .ForMember(z => z.vesselName, y => y.MapFrom(a => a.vesselName))
                     .ForMember(z => z.phase, y => y.MapFrom(a => a.phase))
                     .ForMember(z => z.ata, y => y.MapFrom(a => a.ata))
                     .ForMember(z => z.atd, y => y.MapFrom(a => a.atd))
                     .ForMember(z => z.eta, y => y.MapFrom(a => a.eta))
                     .ForMember(z => z.etd, y => y.MapFrom(a => a.etd))
                     .ForMember(z => z.serv, y => y.MapFrom(a => a.serv));

                x.CreateMap<DB.Container, ContainerViewModel>()
                   .ForMember(z => z.gkey, y => y.MapFrom(a => a.gkey))
                   .ForMember(z => z.equipmentNbr, y => y.MapFrom(a => a.equipmentNbr))
                   .ForMember(z => z.typeIso, y => y.MapFrom(a => a.typeIso))
                   .ForMember(z => z.own, y => y.MapFrom(a => a.own))
                   .ForMember(z => z.typeLength, y => y.MapFrom(a => a.typeLength))
                   .ForMember(z => z.tareWt, y => y.MapFrom(a => a.tareWt))
                   .ForMember(z => z.safeWt, y => y.MapFrom(a => a.safeWt));

                x.CreateMap<ContainerViewModel, DB.ContainerCopia>()
                  .ForMember(z => z.gkey, y => y.MapFrom(a => a.gkey))
                  .ForMember(z => z.equipmentNbr, y => y.MapFrom(a => a.equipmentNbr))
                  .ForMember(z => z.typeIso, y => y.MapFrom(a => a.typeIso))
                  .ForMember(z => z.own, y => y.MapFrom(a => a.own))
                  .ForMember(z => z.typeLength, y => y.MapFrom(a => a.typeLength))
                  .ForMember(z => z.tareWt, y => y.MapFrom(a => a.tareWt))
                  .ForMember(z => z.safeWt, y => y.MapFrom(a => a.safeWt));

                x.CreateMap<DocumentoViewModel, DB.TDocumento1>()
                  .ForMember(z => z.idDocumento, y => y.MapFrom(a => a.idDocumento))
                  .ForMember(z => z.NombreDocumento, y => y.MapFrom(a => a.NombreDocumento))
                  .ForMember(z => z.TipoDocumento, y => y.MapFrom(a => a.TipoDocumento))
                  .ForMember(z => z.Contenido, y => y.MapFrom(a => a.Contenido))
                  .ForMember(z => z.idVessel, y => y.MapFrom(a => a.idVessel));

                x.CreateMap<DB.TDocumento1,DocumentoViewModel>()
                  .ForMember(z => z.idDocumento, y => y.MapFrom(a => a.idDocumento))
                  .ForMember(z => z.NombreDocumento, y => y.MapFrom(a => a.NombreDocumento))
                  .ForMember(z => z.TipoDocumento, y => y.MapFrom(a => a.TipoDocumento))
                  .ForMember(z => z.Contenido, y => y.MapFrom(a => a.Contenido))
                  .ForMember(z => z.idVessel, y => y.MapFrom(a => a.idVessel));

                x.CreateMap<HttpPostedFileBase, DB.TDocumento1>()
                  .ForMember(z => z.NombreDocumento, y => y.MapFrom(a => a.FileName))
                  .ForMember(z => z.TipoDocumento, y => y.MapFrom(a => a.ContentType))
                  .ForMember(z => z.Contenido, y => y.MapFrom( a => ToByteArray(a)));
       
            });
        }

        public static Byte[] ToByteArray(HttpPostedFileBase value)
        {
            if (value == null) return null;
            var array = new Byte[value.ContentLength];
            value.InputStream.Position = 0;
            value.InputStream.Read(array, 0, value.ContentLength);
            return array;
        }

    }
}