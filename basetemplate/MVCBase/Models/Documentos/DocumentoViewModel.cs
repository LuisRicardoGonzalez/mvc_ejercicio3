﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBase.Models.Documentos
{
    public class DocumentoViewModel
    {
        public int idDocumento { get; set; }
        public string NombreDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public byte[] Contenido { get; set; }
        public int idVessel { get; set; }
    }
}