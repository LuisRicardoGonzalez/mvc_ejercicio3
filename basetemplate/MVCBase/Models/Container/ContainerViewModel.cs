﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBase.Models.Container
{
    public class ContainerViewModel
    {
        public int gkey { get; set; }
        public string equipmentNbr { get; set; }
        public string typeIso { get; set; }
        public string own { get; set; }
        public string typeLength { get; set; }
        public int tareWt { get; set; }
        public int safeWt { get; set; }
    }
}