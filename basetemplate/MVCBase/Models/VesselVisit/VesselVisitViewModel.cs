﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MVCBase.Models.VesselVisit
{
    public class VesselVisitViewModel
    {
        //public bool check { get; set; }
        [Display(Name = "ID")]
        public int id { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Visita")]
        public string visit { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Linea")]
        public string line { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Vessel")]
        public string vesselName { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Fase")]
        public string phase { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Tiempo ATA")]
        [DataType(DataType.Date)]
        public DateTime ata { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Tiempo ATD")]
        [DataType(DataType.Date)]
        public DateTime atd { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Tiempo ETA")]
        [DataType(DataType.Date)]
        public DateTime eta { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Tiempo ETD")]
        [DataType(DataType.Date)]
        public DateTime etd { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Servicio")]
        public string serv { get; set; }

    }




}