//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class TDocumento1
    {
        public int idDocumento { get; set; }
        public string NombreDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public byte[] Contenido { get; set; }
        public Nullable<int> idVessel { get; set; }
    }
}
